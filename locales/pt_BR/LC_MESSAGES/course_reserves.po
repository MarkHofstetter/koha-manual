# Compendium of pt_BR.
msgid ""
msgstr ""
"Project-Id-Version: compendium-pt_BR\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-05-15 19:31-0300\n"
"PO-Revision-Date: 2018-05-15 19:52-0300\n"
"Language-Team: Koha Translation Team \n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../source/course_reserves.rst:4
msgid "Course Reserves"
msgstr "Reservas do curso"

#: ../../source/course_reserves.rst:6
msgid ""
"The course reserves module in Koha allows you to temporarily move items to "
"'reserve' and assign different circulation rules to these items while they "
"are being used for a specific course."
msgstr ""
"O módulo de reservas para cursos no Koha permite que você mova "
"temporariamente os exemplares para 'reserva' e defina regras específicas de "
"circulação para estes exemplares enquanto eles estiverem reservados para um "
"curso específico."

#: ../../source/course_reserves.rst:13
msgid "Course Reserves Setup"
msgstr "Configuração de Reservas para Cursos"

#: ../../source/course_reserves.rst:15
msgid "Before using Course Reserves you will need to do some set up."
msgstr ""
"Antes de utilizar as funções de Reservas para cursos, você precisa realizar "
"algumas configurações."

#: ../../source/course_reserves.rst:17
#, fuzzy
msgid ""
"First you will need to enable course reserves by setting the :ref:"
"`UseCourseReserves` preference to 'Use'."
msgstr ""
"Em primeiro lugar, você terá que ativar as reservas para cursos configurando "
"UseCourseReserves para 'Utilizar'."

#: ../../source/course_reserves.rst:20
#, fuzzy
msgid ""
"Next you will need to have all of your course instructors :ref:`added as "
"patrons <add-a-new-patron-label>`."
msgstr ""
"Em seguida você precisará ter todos os instrutores dos cursos adicionados "
"como usuários do sistema."

#: ../../source/course_reserves.rst:23
#, fuzzy
msgid ""
"Next you will want to add a couple of new :ref:`authorized values <add-new-"
"authorized-value-label>` for Departments and Terms."
msgstr ""
"Em seguida você adicionará alguns novos valores autorizados para "
"Departamentos e Termos."

#: ../../source/course_reserves.rst:26
#, fuzzy
msgid ""
"You may also want to create new :ref:`item types <adding-item-types-label>`, "
"`collection codes <#ccode>`__ and/or `shelving locations <#shelvelocvals>`__ "
"to make it clear that the items are on reserve to your patrons. You will "
"also want to be sure to confirm that your :ref:`circulation and fine rules "
"<circulation-and-fine-rules-label>` are right for your new item types "
"(whether they be hourly or daily loans)."
msgstr ""
"Você também pode querer criar novos tipos de material, códigos de coleção e/"
"ou localizações de estante para tornar claro que determinados exemplares "
"estão reservados para seus usuários. Você também pode querer se certificar "
"de que suas regras de circulação e multa estão adequadas para seus novos "
"tipos de material (sejam eles para empréstimos por hora ou dias)."

#: ../../source/course_reserves.rst:36
#, fuzzy
msgid "Adding Courses"
msgstr "Adicionar registros"

#: ../../source/course_reserves.rst:38
msgid ""
"Once you have completed your set up for Course Reserves you can start "
"creating courses and adding titles to the reserve list."
msgstr ""
"Quando completar a configuração das Reservas para Cursos, você poderá "
"começar a criar cursos e adicionar títulos a suas listas de reserva."

#: ../../source/course_reserves.rst:41
msgid "|image726|"
msgstr ""

#: ../../source/course_reserves.rst:43
msgid ""
"From the main course reserves page you can add a new course by clicking the "
"'New course' button at the top left."
msgstr ""
"A partir da página principal de reservas para o curso você pode adicionar um "
"novo curso clicando em 'Novo curso' na barra da esquerda."

#: ../../source/course_reserves.rst:46
msgid "|image727|"
msgstr ""

#: ../../source/course_reserves.rst:48
msgid ""
"Your new course will need a Department, Number and Name at the bare minimum. "
"You can also add in additional details like course section number and term. "
"To link an instructor to this course simply start typing their name and Koha "
"will search your patron database to find you the right person."
msgstr ""
"Seu novo curso precisará no mínimo de um departamento, número e nome. Você "
"também pode incluir informações adicionais como sua sala e duração. Para "
"relacionar um instrutor a este curso, simplesmente comece a escrever seu "
"nome e a busca na base de dados do Koha irá trazer a pessoa correta."

#: ../../source/course_reserves.rst:54
msgid "|image728|"
msgstr ""

#: ../../source/course_reserves.rst:56
msgid ""
"Once the instructor you want appears just click their name and they will be "
"added. You can repeat this for all instructors on this course. Each "
"instructor will appear above the search box and can be removed by clicking "
"the 'Remove' link to the right of their name."
msgstr ""
"Quando aparecer o nome do instrutor, apenas clique em cima e ele será "
"adicionado. Você pode repetir este processo para todos os instrutores neste "
"curso. O nome de cada instrutor irá aparecer sobre a caixa de pesquisa e "
"poderá ser removido através do link 'Remover'."

#: ../../source/course_reserves.rst:61
msgid "|image729|"
msgstr ""

#: ../../source/course_reserves.rst:63
msgid ""
"If you would like your course to show publicly you'll want to be sure to "
"check the 'Enabled?' box before saving your new course."
msgstr ""
"Caso você queira que seu curso tenha as reservas exibidas publicamente, você "
"deve selecionar a caixa 'Permitir' antes de salvar seu novo curso."

#: ../../source/course_reserves.rst:66
msgid ""
"Once your course is saved it will show on the main course reserves page and "
"be searchable by any field in the course."
msgstr ""
"Quando seu curso for salvo, ele será exibido na página de cursos com "
"reservas e se tornará pesquisável através de qualquer dado inserido em seu "
"cadastro."

#: ../../source/course_reserves.rst:69
msgid "|image730|"
msgstr ""

#: ../../source/course_reserves.rst:74
#, fuzzy
msgid "Adding Reserve Materials"
msgstr "Materiais mixtos"

#: ../../source/course_reserves.rst:76
msgid ""
"Before adding reserve materials you will need at least one course to add "
"them to. To add materials visit the Course Reserves module."
msgstr ""
"Antes de incluir materiais na reserva você precisa de ao menos um curso para "
"adicioná-los. Para adicionar materiais acesse o módulo Reservas de Cursos."

#: ../../source/course_reserves.rst:79
msgid "|image731|"
msgstr ""

#: ../../source/course_reserves.rst:81
msgid "Click on the title of the course you would like to add materials to."
msgstr "Clique no título do curso para o qual você deseja adicionar materiais."

#: ../../source/course_reserves.rst:83
msgid "|image732|"
msgstr ""

#: ../../source/course_reserves.rst:85
msgid ""
"At the top of the course description click the 'Add reserves' button to add "
"titles to this reserve list. You will be asked to enter the barcode for the "
"reserve item."
msgstr ""
"No cabeçalho da descrição do curso clique em 'Adicionar reservas' para "
"incluir títulos a esta lista de reservas. Será solicitado o código de barras "
"do exemplar."

#: ../../source/course_reserves.rst:89
msgid "|image733|"
msgstr ""

#: ../../source/course_reserves.rst:91
#, fuzzy
msgid ""
"After you are done scanning the barcodes to add to the course you can see "
"them on the course page"
msgstr ""
"Depois de escanear o código de barras para adicioná-los ao curso, você pode "
"vê-los na página do curso."

#: ../../source/course_reserves.rst:94
msgid "|image734|"
msgstr ""

#: ../../source/course_reserves.rst:99
#, fuzzy
msgid "Course Reserves in the OPAC"
msgstr "Reservas do curso"

#: ../../source/course_reserves.rst:101
msgid ""
"Once you have enabled Course Reserves and added courses you will see a link "
"to Course Reserves below your search box in the OPAC."
msgstr ""

#: ../../source/course_reserves.rst:104
msgid "|image735|"
msgstr ""

#: ../../source/course_reserves.rst:106
msgid ""
"Clicking that link will show you your list of enabled courses (if you have "
"only one course you will just see the contents of that one course)."
msgstr ""

#: ../../source/course_reserves.rst:109
msgid "|image736|"
msgstr ""

#: ../../source/course_reserves.rst:111
msgid ""
"You can search course reserves by any field (course number, course name, "
"instructor name, department) that is visible in the list of courses. "
"Clicking a course name will show you the details and reserve items."
msgstr ""

#: ../../source/course_reserves.rst:115
msgid "|image737|"
msgstr ""
